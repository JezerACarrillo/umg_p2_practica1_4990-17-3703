package practicano1;

import java.util.Scanner;

public class PracticaNo1 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.println("Menú Principal");
        System.out.println("1. INGRESO Y TRATAMIENTO DE NOMBRES");
        System.out.println("2. CALCULO DE AREAS Y DIAMETROS");
        System.out.println("3. SALIR DEL SISTEMA");

        int Opcion = entrada.nextInt();

        int area;
        int diametro;

        int radioe;
        int largoc;
        int radioc;
        int largocu;

        double pi = 3.1416;

        if (Opcion == 1) {

            System.out.println("Persona No1");

            System.out.println("Primer Nombre");
            String nombre1 = entrada.next();

            System.out.println("Segundo Nombre");
            String segnombre1 = entrada.next();

            System.out.println("Primer Apellido");
            String apellido1 = entrada.next();

            System.out.println("Segundo Apellido");
            String segapellido1 = entrada.next();

            System.out.println("Persona No2");

            System.out.println("Primer Nombre");
            String nombre2 = entrada.next();

            System.out.println("Segundo Nombre");
            String segnombre2 = entrada.next();

            System.out.println("Primer Apellido");
            String apellido2 = entrada.next();

            System.out.println("Segundo Apellido");
            String segapellido2 = entrada.next();

            System.out.println("Persona No3");

            System.out.println("Primer Nombre");
            String nombre3 = entrada.next();

            System.out.println("Segundo Nombre");
            String segnombre3 = entrada.next();

            System.out.println("Primer Apellido");
            String apellido3 = entrada.next();

            System.out.println("Segundo Apellido");
            String segapellido3 = entrada.next();

            System.out.println("Perosna No1: " + apellido1 + " " + segapellido1 + " " + "," + " " + nombre1 + " " + segnombre1);
            System.out.println("Perosna No2: " + apellido2 + " " + segapellido2 + " " + "," + " " + nombre2 + " " + segnombre2);
            System.out.println("Perosna No3: " + apellido3 + " " + segapellido3 + " " + "," + " " + nombre3 + " " + segnombre3);

        } else if (Opcion == 2) {

            System.out.println("1. Calculo de área");
            System.out.println("2. Calculo de diámetro");

            int Opcion2 = entrada.nextInt();

            if (Opcion2 == 1) {

                System.out.println("1. Esfera");
                System.out.println("2. Cubo");
                System.out.println("3. Circulo");
                System.out.println("4. Cuadrado");

                int Opcion3 = entrada.nextInt();

                if (Opcion3 == 1) {
                    System.out.println("Ingrese el radio de la Esfera");
                    radioe = entrada.nextInt();

                    area = (int) (4 * pi * radioe * radioe);

                    System.out.println("El área de la esfera es: " + area);

                }
                if (Opcion3 == 2) {
                    System.out.println("Ingrese el largo del Cubo");
                    largoc = entrada.nextInt();

                    area = 6 * largoc * largoc;
                    System.out.println("El área del cubo es: " + area);

                }
                if (Opcion3 == 3) {
                    System.out.println("Ingrese el radio del Circulo");
                    radioc = entrada.nextInt();

                    area = (int) (pi * radioc * radioc);
                    System.out.println("El área del circulo es: " + area);

                }
                if (Opcion3 == 4) {
                    System.out.println("Ingrese el largo del Cuadrado");
                    largocu = entrada.nextInt();

                    area = largocu * largocu;
                    System.out.println("El área del cuadrado es: " + area);

                }

            } else if (Opcion2 == 2) {

                System.out.println("1. Esfera");
                System.out.println("2. Cubo");
                System.out.println("3. Circulo");
                System.out.println("4. Cuadrado");

                int Opcion4 = entrada.nextInt();

                if (Opcion4 == 1) {
                    System.out.println("Ingrese el radio de la Esfera");
                    radioe = entrada.nextInt();

                    diametro = (int) (1.3333 * pi * (radioe * radioe * radioe));

                    System.out.println("El diametro de la esfera es: " + diametro);

                }
                if (Opcion4 == 2) {
                    System.out.println("Ingrese el largo del Cubo");
                    largoc = entrada.nextInt();

                    diametro = largoc * largoc * largoc;

                    System.out.println("El diametro del cuadrado es: " + diametro);
                }
                if (Opcion4 == 3) {
                    System.out.println("Ingrese el radio del Circulo");
                    radioc = entrada.nextInt();

                    diametro = (int) (2 * pi * radioc);

                    System.out.println("El diametro del circulo es: " + diametro);

                }
                if (Opcion4 == 4) {
                    System.out.println("Ingrese el largo del Cuadrado");
                    largocu = entrada.nextInt();

                    diametro = largocu * 4;
                    System.out.println("El diametro del cuadrado es: " + diametro);

                }
            }

        }

    }
}
